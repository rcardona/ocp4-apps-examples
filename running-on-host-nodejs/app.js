var express = require('express');
app = express();
var os = require("os");
var hostname = os.hostname();

app.get('/', function (req, res) {
  res.send(' Hostname: ' + JSON.stringify(hostname) + '\n');
});

app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});
