- create service account
> oc create sa pseudo-root-sa
>

- give the service account privileged access
> oc adm policy add-scc-to-user privileged -z pseudo-root-sa
>

- create physical volume claim

      echo -e 'apiVersion: v1
      kind: PersistentVolumeClaim
      metadata:
       name: conda-claim
      spec:
        accessModes:
          - ReadWriteOnce
        storageClassName: gp2
        volumeMode: Filesystem
        resources:
          requests:
            storage: 100Gi' > 0-anaconda3-pvc.yaml

- create pod definition

      echo -e 'apiVersion: v1
      kind: Pod
      metadata:
        name: anaconda3
        labels:
          app: conda
      spec:
        volumes:
          - name: conda-volume
            persistentVolumeClaim:
             claimName: conda-claim
        containers:
        - image: docker.io/continuumio/anaconda3
          name: conda
          serviceAccountName: pseudo-root-sa
          serviceAccount: pseudo-root-sa
          securityContext:
            runAsUser: 0
          env:
            - name: JUPYTERCMD
              value: "/opt/conda/bin/conda install jupyter nb_conda -y --quiet && /opt/conda/bin/jupyter notebook --notebook-dir=/opt/notebooks --ip='*' --port=8888 --no-browser --allow-root"
          command: ["bash"]
          args: ["-c","$(JUPYTERCMD)"]
          ports:
            - containerPort: 8888
              name: "http-server"
          resources:
            limits:
              nvidia.com/gpu: 1
          volumeMounts:
              - mountPath: "/opt/notebooks"
                name: conda-volume' > 1-anaconda3-pod-definition.yaml

- create service

      echo -e "apiVersion: v1
      kind: Service
      metadata:
       name: anaconda3
      spec:
       type: ClusterIP
       ports:
         - targetPort: 8888
           port: 8888
           name: web
       selector:
         app: conda" > 2-anaconda3-svc.yalm



#
