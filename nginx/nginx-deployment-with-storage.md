- 0 - create a project, ex *test*
> $ oc new-project test
>

- 1 - create physical volume claim

      echo -n 'apiVersion: v1
      kind: PersistentVolumeClaim
      metadata:
        name: nginx-sample-pv-claim
      spec:
        storageClassName: ocs-storagecluster-ceph-rbd
        accessModes:
          - ReadWriteOnce
        resources:
          requests:
            storage: 10Mi' | tee 1-pvc-nginx-simple.yaml

   > $ oc apply -f 1-pvc-nginx-simple.yaml
   >

- 2 - create the deployment

      echo -n 'apiVersion: apps/v1
      kind: Deployment
      metadata:
        labels:
          app: nginx-stable
        name: nginx-stable
      spec:
        replicas: 1
        selector:
          matchLabels:
            app: nginx-stable
        template:
          metadata:
            labels:
              app: nginx-stable
          spec:
            containers:
            - image: twalter/openshift-nginx@sha256:b6a09b124c14ea2bb81f6081fb0d6e4f5214b9a1c98f6173fffc8e07d2382ca3
              name: nginx-stable
              ports:
              - containerPort: 80
                protocol: TCP
              - containerPort: 8081
                protocol: TCP
              volumeMounts:
              - mountPath: "/usr/share/nginx/html"
                name: nginx-sample-pv
            volumes:
              - name: nginx-sample-pv
                persistentVolumeClaim:
                  claimName: nginx-sample-pv-claim' | tee 2-deployment-nginx-simple.yaml

    > $ oc apply -f 2-deployment-nginx-simple.yaml
    >

- 3 - create service

      echo -n 'apiVersion: v1
      kind: Service
      metadata:
        labels:
          app: nginx-stable
        name: nginx-stable
      spec:
        selector:
          app: nginx-stable
        ports:
        - name: 80-tcp
          port: 80
          protocol: TCP
          targetPort: 80
        - name: 8081-tcp
          port: 8081
          protocol: TCP
          targetPort: 8081
        type: ClusterIP' | tee 3-service-nginx-simple.yaml

    > $ oc apply -f 3-service-nginx-simple.yaml
    >

- 4 - create route

      echo -n 'apiVersion: v1
      kind: List
      metadata:
      items:
      - apiVersion: route.openshift.io/v1
        kind: Route
        metadata:
          labels:
            app: nginx-stable
          name: nginx-stable
        spec:
          port:
            targetPort: 8081
          to:
            kind: Service
            name: nginx-stable' | tee 4-route-nginx-simple.yaml

   > $ oc apply -f 4-route-nginx-simple.yaml
   >
    
- 5 - (optional) create an index to the nginx webserver running on the application pods

> $ oc rsh nginx-stable-5b75957d74-pjn7p
> 
> 1000740000@nginx-stable-5b75957d74-pjn7p:/$ echo "MySite" > /usr/share/nginx/html/index.html
>

---
