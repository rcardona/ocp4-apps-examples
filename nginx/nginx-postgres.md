- create project
```shell
oc new-project t1
```

- deploy postgres db instance
```shell
oc new-app \
-e POSTGRESQL_ADMIN_PASSWORD=postgres \
-e DATABASE_SERVICE_NAME=contactsdb \
-e POSTGRESQL_USER=contacts \
-e POSTGRESQL_PASSWORD=contacts \
-e POSTGRESQL_DATABASE=contactsdb \
postgresql:13-el7
```
- create the db secret
```shell
oc create secret generic contactsdb-secret \
--from-literal=DB_CONFIG="postgresql://contacts:contacts@postgresql:5432/contactsdb"
```

- deploy application in web console

  - 0 - from the _developer view_, Select __Add__, and then click __From Catalog__

  - 1 - select _Languages_ > _JavaScript_, and then click the _Node.js_ builder image option, and create _Create Application_

  - 2 - fill this information in the __Git__ section

    - Git Repo URL: https://github.com/RedHatTraining/DO101-apps

    - Git reference: main

    - Context dir: /contacts

  - 3 - fill this information in the __General__ section

    - Application name: contacts

    - Name: contacts

  - 4 - click on the __Deployment__ Advanced options and add Environment variables (__Add from ConfigMap or Secret__)

    - Name: DB_CONFIG

    - Resource: contactsdb-secret

    - Key: DB_CONFIG

  - finally create the application

  - ![How to do the deployment](file/app-deployment.mp4)

---


