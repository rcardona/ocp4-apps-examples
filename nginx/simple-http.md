## Run it locally with podman

- create the index file
  ```text
  echo -e '<!DOCTYPE html>
  <html lang="en">
  <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Hello World - Nginx Docker</title>
      <style>
          h1{
              font-weight:lighter;
              font-family: Arial, Helvetica, sans-serif;
          }
      </style>
  </head>
  <body>

      <h1>
          Making Linux Great again!
      </h1>

  </body>
  </html>' | tee index.html
  ```

- create the nginx Dockerfile
  ```text
  echo -e 'FROM registry.redhat.io/ubi9/httpd-24:latest
  COPY index.html /var/www/html/index.html' | tee Dockerfile
  ```

- build simple nginx container image
  ```shell
  podman build . -t simple-nginx
  ```

- then run the container
  ```shell
  podman run --rm -it -p 8080:80 simple-nginx
  ```

- with workaround
  ```shell
  podman --runtime /usr/bin/crun run -p 8080:80 simple-nginx
  ```

## Run it OpenShift

- locate the ocp cluster internal registry, and login to it
  ```shell
  podman login -u $(oc whoami) -p $(oc whoami -t) \
  $(oc -n openshift-image-registry get route default-route -o jsonpath='{.spec.host}') \
  --tls-verify=false
  ```

- make sure that the default route is enabled
  ```shell
  oc patch configs.imageregistry.operator.openshift.io/cluster --patch '{"spec":{"defaultRoute":true}}' --type=merge
  ```  

- give the image a proper tag in order to match the target registry. Here bellow `openshift` is the target project to make the image available to all project
  ```shell
  podman tag localhost/simple-nginx \
  $(oc -n openshift-image-registry get route default-route -o jsonpath='{.spec.host}')/openshift/simple-nginx
  ```

- push image to OCP registry
  ```shell
  podman push \
  $(oc -n openshift-image-registry get route default-route -o jsonpath='{.spec.host}')/openshift/simple-nginx \
  --tls-verify=false
  ```

- fine the exact name for the image in the internal registry
  ```shell
  oc get images | grep -i simple
  ```

- run the application
  ```shell
  oc new-app openshift/simple-nginx
  ```

- create route to expose the application externally
  ```shell
  oc expose svc simple-nginx
  ```

---
