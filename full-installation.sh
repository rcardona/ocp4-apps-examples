#!/usr/bin/env bash

set -euo pipefail
set -x

# VC API Variables
export GOVC_URL='{{ VC-IP }}'
export GOVC_USERNAME='administrator@vsphere.local'
export GOVC_PASSWORD='VMware01!'
export GOVC_INSECURE=1
export GOVC_DATACENTER="{{ omi-datacenter }}"

# Infrastructure resources
export FILESERVER="http://10.10.10.10:8080"
export DNSSERVER="10.10.10.10"
export GATEWAY="10.10.10.10"
export NETMASK="27"
export DATESTORE="3TB LOB"
export NETWORK_VM="VM Network 4"
export OCP_VM_TEMPLATE="cp4-node-template"

# Cluster Variables
export basedomain="my.labs"
export clustername="ocp44vmw"

nodes=(
    "bootstrap.${clustername}.${basedomain}"
    "master-0".${clustername}.${basedomain}"
    "master-1".${clustername}.${basedomain}"
    "master-2".${clustername}.${basedomain}"
    "worker-0".${clustername}.${basedomain}"
    "worker-1".${clustername}.${basedomain}"
    "worker-2".${clustername}.${basedomain}"
)

ignitions=(
    'bootstrap.ign'
    'master.ign'
    'master.ign'
    'master.ign'
    'worker.ign'
    'worker.ign'
);

ips=(
    '10.10.10.10'
    '10.10.10.11'
    '10.10.10.12'
    '10.10.10.13'
    '10.10.10.14'
    '10.10.10.15'
)

# Setup vm's
for (( i=0; i< ${#nodes[@]} ; i++ )) ; do
    node=${nodes[$i]}
    ip=${ips[$i]}
    ignition=${ignitions[$i]}

    echo "Setup $node -> $ip";

    # If you want to setup mac adress
    govc vm.clone -vm "/${GOVC_DATACENTER}/vm/${clustername}/${OCP_VM_TEMPLATE}"  \
        -annotation="${node}" \
        -c=4 \
        -m=16384 \
        -net "${NETWORK_VM}" \
        -on=false \
        -folder="${clustername}" \
        -ds="${DATASTORE}" \
        ${node}

    # iPXE settings
    govc vm.change \
        -e="guestinfo.ipxe.hostname=${node}" \
        -e="guestinfo.ipxe.ignition=${ignition}" \
        -e="guestinfo.ipxe.net0.ip=${ip}" \
        -e="guestinfo.ipxe.net0.netmask=${NETMASK}" \
        -e="guestinfo.ipxe.net0.gateway=${GATEWAY}" \
        -e="guestinfo.ipxe.fileserver=${FILESERVER}" \
        -e="guestinfo.ipxe.dns=${DNSSERVER}" \
        -e="guestinfo.ipxe.kernel-installer=rhcos-4.4.3-x86_64-installer-kernel-x86_64" \
        -e="guestinfo.ipxe.initrd-installer=rhcos-4.4.3-x86_64-installer-initramfs.x86_64.img" \
        -e="guestinfo.ipxe.rhcos-image=rhcos-4.4.3-x86_64-metal.x86_64.raw.gz" \
        -e="guestinfo.ipxe.disk=sda" \
        -e="guestinfo.ipxe.net_interface=ens192" \
        -vm="/${GOVC_DATACENTER}/vm/${clustername}/${node}"
done;

# Start vm's
for node in ${nodes[@]} ; do
    echo "# Start $node";
    govc vm.power -on=true $node
done;
