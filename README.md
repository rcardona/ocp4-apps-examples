## README

- Working nginx simple server tested on 4.12 (simple-nginx)
  > $ oc new-app twalter/openshift-nginx:stable --name nginx-stable
  >
  > $ oc expose svc nginx-stable --port=8081


- Run hello-openshift
  > $oc new-app https://gitlab.com/rcardona/ocp4-apps-examples.git --context-dir=hello-openshift --name my-test-app

- Run running-on-host-nodejs
  > $oc new-app nodejs:10~https://gitlab.com/rcardona/ocp4-apps-examples.git --context-dir=running-on-host-nodejs --name my-test-app

- Simple django app
  > $oc new-app django-psql-example
