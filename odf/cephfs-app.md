#### Use this example to create applications that use persistent storage with ODF Cephfs

The applications are divided in case(s). In order to appreciate the functionatlity make sure project **image-tool** does not exist before execuing any of the cases.

---
#### Case 0: aplication with ephemeral storage
- 0.1. create project
```bash
$ oc new-project image-tool
```
- 0.2. create service account
```bash
$ oc apply -f https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/serviceaccount.yaml
```
- 0.3. create deployment
```bash
$ oc apply -f  https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/ephemeral/0-deployment.yaml
```
- 0.4. create service
```bash
$ oc apply -f  https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/ephemeral/1-service.yaml
```
- 0.5.  create route endpoint
```bash
$ oc apply -f  https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/ephemeral/2-route.yaml
```
---
#### Case 1: create the web application with persistent storage backed by cephfs
- 1.0. create project
```bash
$ oc new-project image-tool
```
- 1.1. create service account
```bash
$ oc apply -f https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/serviceaccount.yaml
```
- 1.2. create persisten volume clam backed the file system base storage **ocs-storagecluster-cephfs**
```bash
$ oc apply -f https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/pvc.yaml
```
- 1.3. create deployment
```bash
$ oc apply -f  https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/persistent-webapp/0.0-deployment.yaml
```
- 1.4. create service
```bash
$ oc apply -f  https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/persistent-webapp/0.1-service.yaml
```
- 1.5.  create route endpoint
```bash
$ oc apply -f  https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/persistent-webapp/0.2-route.yaml
```

#### Case 2: this case aims to test the share storage capabilities of the storage class ocs-storagecluster-cephfs. It requieres that Case 1 is previously completed.
- 2.0. create deployment
```bash
$ oc apply -f  https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/persistent-webapp/1.0-deployment.yaml
```
- 2.1. create service
```bash
$ oc apply -f  https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/persistent-webapp/1.1-service.yaml
```
- 2.2.  create route endpoint
```bash
oc apply -f  https://gitlab.com/rcardona/ocp4-apps-examples/-/raw/master/odf/files/persistent-webapp/1.2-route.yaml
```
