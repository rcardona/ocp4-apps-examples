#### 0 - find the s3 endpoint of Openshift Data Foundation
  > $ oc get routes -n openshift-storage | grep s3
  >

    user@provision ~]$ oc get routes -n openshift-storage | grep s3
    s3       s3-openshift-storage.apps.ocp.example.com            s3            s3-https     reencrypt/Allow      None

#### 1 -  create simple bucket
  > $ oc new-project test-project
  >

    echo -n 'apiVersion: objectbucket.io/v1alpha1
    kind: ObjectBucketClaim
    metadata:
      name: test-bucket
      namespace: test-project
    spec:
      additionalConfig:
        bucketclass: noobaa-default-bucket-class
      generateBucketName: test
      storageClassName: openshift-storage.noobaa.io' | tee obc.yaml

   > $ oc apply -f tee obc.yaml
   >

#### 2 -  extract bucket credentials

    echo -e "[default]
    aws_access_key_id=`oc get -o json secret test-bucket | jq -r '.data.AWS_ACCESS_KEY_ID' | base64 -d`
    aws_secret_access_key=`oc get -o json secret test-bucket | jq -r '.data.AWS_SECRET_ACCESS_KEY' | base64 -d`" | tee credentials-velero

   > $ cat credentials-velero
   >

    [default]
    aws_access_key_id=mj0FpmnxpySh2Pbu53TE
    aws_secret_access_key=MEwh+0THzBK3Xsym+GjpgKrS+VNQZ/WE/PrZr65R

#### 3 - list the bucket via the s3 endpoint with aws program

   - set the bucket credentials (step 3) in the environment
   > $ aws configure
   >

   - list buckets
   > $ aws s3 --endpoint-url http://s3-openshift-storage.apps.ocp.example.com ls
   >

   - list contents of the buckets
   > $ aws s3 --endpoint-url http://s3-openshift-storage.apps.ocp.example.com ls s3://test-0e0643cd-03f9-4beb-b5ae-3a90c47ffe97
   >

#### 4 - upload object to bucket via the s3 endpoint with aws program
  > $ aws s3 --endpoint-url http://s3-openshift-storage.apps.ocp.example.com cp ./tilo.txt s3://test-0e0643cd-03f9-4beb-b5ae-3a90c47ffe97
  >

#### 5 - download object to bucket via the s3 endpoint with aws program
  > $ aws s3 --endpoint-url http://s3-openshift-storage.apps.ocp.example.com cp s3://test-0e0643cd-03f9-4beb-b5ae-3a90c47ffe97/tilo.txt ./
  >
  