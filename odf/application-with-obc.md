- create ObjectBucketClaim

      $ cat obc.yaml

      apiVersion: objectbucket.io/v1alpha1
      kind: ObjectBucketClaim
      metadata:
        name: CHANGE_ME
        namespace: CHANGE_ME
      spec:
        additionalConfig:
          bucketclass: noobaa-default-bucket-class
        generateBucketName: CHANGE_ME
        storageClassName: CHANGE_ME

- check the resource has been properly created and bounded
  > $ oc get obc
  >


- extract the secret from the newly created obc
  > oc get secret
  >
      $ obc-secret

  > $ oc extract secret/obc-secret
  >

      AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY

- create secret for application
  > $ oc create secret generic my-app-secret --from-literal=AWS_ACCESS_KEY_ID="\$(cat AWS_ACCESS_KEY_ID)" --from-literal=AWS_SECRET_ACCESS_KEY="\$(cat AWS_SECRET_ACCESS_KEY)"
  >

- tbc with application
